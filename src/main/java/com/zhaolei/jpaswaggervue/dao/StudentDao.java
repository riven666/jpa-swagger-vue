package com.zhaolei.jpaswaggervue.dao;

import com.zhaolei.jpaswaggervue.pojo.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author 15579
 * 2019/6/3 14:21
 * 文件说明：StudentDao层
 */
@Repository
public interface StudentDao extends JpaRepository<Student,Integer>, JpaSpecificationExecutor<Student>{
    //nativeQuery =true 表示这是原生SQL
    @Query(value = "SELECT * from student where `name` like concat('%',?,'%')",nativeQuery = true)
    Page<Student> findLikeNameByPage(String name, Pageable pageable);


//    @Query(value = "SELECT new com.zhaolei.springdatajpa.Util.FindResultUtil(s.id,s.name,s.sex,s.gradeId,g.gradeName) FROM student as s,grade as g\n" +
//            "where s.gradeId=g.id and s.name LIKE concat('%',#{stuName},'%')")
//    Page<FindResultUtil>  findStudentByGrade(@Param("stuName") String name, Pageable pageable);
}
