package com.zhaolei.jpaswaggervue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2      //开启swagger2接口文档
public class JpaSwaggerVueApplication {
    public static void main(String[] args) {
        System.out.println("hello  SpringBoot");
        SpringApplication.run(JpaSwaggerVueApplication.class, args);
    }
}
