package com.zhaolei.jpaswaggervue.service.impl;

import com.zhaolei.jpaswaggervue.dao.StudentDao;
import com.zhaolei.jpaswaggervue.pojo.Student;
import com.zhaolei.jpaswaggervue.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 15579
 * 2019/6/4 14:27
 * 文件说明：
 */
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentDao studentDao;

    /**
     * 新增学生信息
     *
     * @param student
     * @return
     */
    @Override
    public Student save(Student student) {
        return studentDao.save(student);
    }

    /**
     * 修改
     *
     * @param student
     * @return
     */
    @Override
    public Student update(Student student) {
        return studentDao.save(student);
    }

    /**
     * 删除学生
     *
     * @param id
     */
    @Override
    public void deleteStu(Integer id) {
        studentDao.deleteById(id);
    }

    /**
     * 分页查询
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Page<Student> findByPage(Integer pageNum, Integer pageSize,Student student) {
        if(pageNum==null||pageNum==0){
            pageNum=1;
        }
        if(pageSize==null||pageSize==0){
            pageSize=2;
        }
//        第一种方式模糊查询并且分页
//        ExampleMatcher matcher = ExampleMatcher.matching()
//                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.startsWith())//模糊查询匹配开头，即{username}%
//                .withIgnorePaths("sex")
//                .withIgnorePaths("id");
//        Example<Student> example = Example.of(student, matcher);

        PageRequest of=PageRequest.of(pageNum-1,pageSize);
        //第一种方式
        // Page<Student> page=studentDao.findAll(example,of);
        //第二种方式模糊查询并且分页
        Page<Student> page=studentDao.findLikeNameByPage(student.getName()==null?"":student.getName(),of);
        return page;
    }

    /**
     * 根据编号查询
     *
     * @param id
     * @return
     */
    @Override
    public Student findStudentById(Integer id) {
        return studentDao.findById(id).get();
    }
}
