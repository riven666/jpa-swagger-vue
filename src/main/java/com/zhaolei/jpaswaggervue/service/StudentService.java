package com.zhaolei.jpaswaggervue.service;

import com.zhaolei.jpaswaggervue.pojo.Student;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author 15579
 * 2019/6/4 14:26
 * 文件说明：
 */
public interface StudentService {
    /**
     * 新增学生信息
     * @param student
     * @return
     */
    Student save(Student student);

    /**
     * 修改
     * @param student
     * @return
     */
    Student update(Student student);

    /**
     * 删除学生
     * @param id
     */
    void deleteStu(Integer id);

    /**
     * 分页查询
     * @param pageNum
     * @param pageSize
     * @return
     */
    Page<Student> findByPage(Integer pageNum, Integer pageSize, Student student);

    /**
     * 根据编号查询
     * @param id
     * @return
     */
    Student findStudentById(Integer id);
}
