package com.zhaolei.jpaswaggervue.pojo;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author 15579
 * 2019/6/3 14:09
 * 文件说明：Student实体类
 */
@Data
@Table
@Entity //数据库没有此实体类对应的表就会自动生成
public class Student implements Serializable {
    //自动增长
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @Column
    private String sex;

    @Column
    private  Integer gradeId;
}
